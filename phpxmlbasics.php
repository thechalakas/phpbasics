<?php

echo "<p>we are here and php basics is beginning</p>";
$sourceFile="sampledata.xml";

$xml=simplexml_load_file($sourceFile) or die("Error: Cannot create object parameter");
echo "<p></p>";
echo "<p>This is the first  type of loading xml file</p>";
print_r($xml);
$xml2=simplexml_load_file("sampledata.xml") or die("Error: Cannot create object direct file path");
echo "<p>This is the second type of loading xml file</p>";
print_r($xml2);
$movies = $xml;
echo "<p>movies vairable has $movies</p>";

echo $movies->movie[0]->plot;

echo "<p>Adding a character</p>";
$character = $movies->movie[0]->characters->addChild('character');
$character->addChild('name', 'Mr. Parser');
$character->addChild('actor', 'John Doe');

$rating = $movies->movie[0]->addChild('rating', 'PG');
$rating->addAttribute('type', 'mpaa');

echo "<p>Compare the first xml output and second and notice a new character is added</p>";
print_r($xml);
//orders->saveXML(’orders.xml’);
$xml->saveXML("sampledata.xml");
echo "<p>we are here and php basics is ending</p>"; 
?>