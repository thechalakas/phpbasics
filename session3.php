<?php
session_start();
?>
<!DOCTYPE html>
<html>
<body>

<?php
// Echo session variables that were set on previous page
echo "Favorite color is " . $_SESSION["favcolor"] . ".<br>";
echo "Favorite animal is " . $_SESSION["favanimal"] . ".";

if (isset($_SESSION['favcolor'])) 
{
    $name = $_SESSION['favcolor'];
    echo "<p>$name</p>"; 
}
?>

<input id="name" type="text" name="name" 
<?php 
if (isset($_SESSION['favcolor'])) 
{
    $name = $_SESSION['favcolor'];
    echo "value='$name'"; 
}
else
{
    $name = "color removed";
    echo "value='$name'"; 
}
?> 
>



</body>
</html>