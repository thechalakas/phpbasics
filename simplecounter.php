<?php
//name of cookie
$cookie_name = "counter";
//variable I wish to store in the initial. 
$counter = 1;
$cookie_value = $counter;
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
?>
<!DOCTYPE html>
<html>
<title>HTML Tutorial</title>
<body>

<h1>This is a heading</h1>
<p>This is a paragraph.</p>
<?php 
    echo "<p>-----------------php has begun</p>";
    
    if (isset($_COOKIE['counter'])) 
    {
        echo "<p>entering the if for counter cookie test</p>";
       $counter = (int) $_COOKIE['counter'];
       echo "<p>leaving the if for counter cookie test</p>";
    }
    else
    {
        echo "<p>cookie not set</p>";
    }
    echo "<p> You have been here $counter time(s) recently</p>";
    setcookie('counter', $counter+1, time()+3600, "/");
    echo "<p>php has ended-------------------</p>";
?>

<form action="welcome.php" method="post">
Name: <input type="text" name="name"><br>
E-mail: <input type="text" name="email"><br>
<input type="submit">
</form>
</body>
</html>